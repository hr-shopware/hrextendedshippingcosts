<?php

namespace HrExtendedShippingCosts\Components\Api\Resource;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\QueryBuilder;
use HrExtendedShippingCosts\Models\ExtendedShippingCosts;
use Shopware\Components\Api\Exception\PrivilegeException;
use Shopware\Components\Api\Resource\Resource;

class ExtendedShippingCostApi extends Resource
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     *
     * @param int $month
     * @param int $year
     *
     * @return array
     * @throws PrivilegeException|Exception
     */
    public function getList(int $month, int $year) :array
    {
        $this->checkPrivilege('read');
        $date = "$year-$month-01";
        $qb = $this->connection->createQueryBuilder();
        $qb->select(['*'])->from('hr_extended_shipping_costs', 'esc');
        $qb->andWhere('esc.order_date BETWEEN :start AND :end');
        $qb->setParameter('start', "$date 00:00:00");
        $qb->setParameter('end', date('Y-m-t 00:00:00', strtotime($date)));
        $shippingCosts = $qb->execute()->fetchAll();

        return [
            $shippingCosts,
        ];
    }

    public function getRepository() :QueryBuilder
    {
        return $this->getManager()->getRepository(ExtendedShippingCosts::class)->createQueryBuilder('esc');
    }
}
