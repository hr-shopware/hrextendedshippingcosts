<?php
declare(strict_types=1);

namespace HrExtendedShippingCosts\Components;

class HRConst
{
    /**
     * HrExtendedShippingCosts\HrExtendedShippingCosts.php
     */
    /* main table for shipping entry */
    public const SHIPPING_COST_TABLE = 's_premium_dispatch';
    /* table containing attribute to identify shipping entry */
    public const SHIPPING_COST_ATTR_TABLE = 's_premium_dispatch_attributes';
    /* column-name for attribute that identifies the shipping cost entry */
    public const SHIPPING_COST_ATTR_COLUMN = 'hr_extended_shippingcost';
    /* table to define in which country the shippingcost exist  */
    public const SHIPPING_COST_COUNTRY_TABLE = 's_premium_dispatch_countries';
    /* table where all active payments connected to shippingcosts */
    public const SHIPPING_COST_PAYMENT_TABLE = 's_premium_dispatch_paymentmeans';
    /* table for detailed information like prices and factor and so on */
    public const SHIPPING_COST_DETAIL_TABLE = 's_premium_shippingcosts';
    /* name for shippingcost entry */
    public const SHIPPING_COST_NAME = 'HR Erweiterter Versand';
    /* table to retrieve active payments */
    public const PAYMENT_TABLE = 's_core_paymentmeans';
    /**
     * HrExtendedShippingCosts\Subscriber\FinishCheckoutSubscriber.php
     */
    /* attribute containing main shippingcosts for each product */
    public const SHIPPING_COSTS = 'shippingcost';
    /* column containing value when shippingcosts should be free */
    public const SHIPPING_FREE_FROM = 'shippingfreefrom';
    /* column containing additional shippingcost */
    public const ADDITIONAL_SHIPPING_COST = 'additionalshippingcosts';
    /* column containing boolean value if additional shippingcosts should be applied */
    public const ADDING_UP_SHIPPING_COSTS = 'addingupshippingcosts';
}
