<?php
declare(strict_types=1);

namespace HrExtendedShippingCosts\Components;

class Utils
{
    public static function debug($var, $die = false, $header = "BOF"): void
    {
        echo "<h1>##> {$header} <##</h1><pre>";

        if (is_array($var)) {
            print_r($var);
        }

        if (is_object($var)) {
            var_dump($var);
        }

        if (is_string($var)) {
            echo $var;
        }

        echo "</pre>";

        if ($die) {
            die('#EOF Debug');
        }
    }
}
