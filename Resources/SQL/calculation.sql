/* final select gets total costs from second subselect - just a simple sum select from subselect */
(
    SELECT COALESCE(SUM(total.costs), 0) FROM
        /* second subselect reduces the table data by handling the add sum or max costs */
        (SELECT IF(MAX(X.addingupshippingcosts) = 1, SUM(X.totalshippingcosts), MAX(X.totalshippingcosts)) AS costs,
                X.sessionID
        FROM
             /* first subselect that goes through basket and gathers all necessary infos and return a table with supplierID, addingupshippingcosts and costs */
             ( SELECT a.supplierID, asa.addingupshippingcosts, ob.sessionID,
                  IF(
                       /* check if total sum of articles is larger than shipping-free-border (shippingfreefrom) */
                       asa.shippingfreefrom > 0 AND
                       (SELECT SUM(sub_ob.price * sub_ob.quantity) FROM s_order_basket sub_ob
                            JOIN s_articles sub_a ON sub_a.id = sub_ob.articleID
                            JOIN s_articles_details sub_d ON sub_d.articleID = sub_a.id AND sub_d.ordernumber = sub_ob.ordernumber
                            JOIN s_articles_attributes sub_aa ON sub_aa.articledetailsID = sub_d.id
                       WHERE sub_ob.sessionID = ob.sessionID AND sub_aa.attr10 = aa.attr10
                            AND sub_a.supplierID = a.supplierID) >= asa.shippingfreefrom
                       /* check if article is shipping free */
                       OR ob.shippingfree,
                       /* when previous checks came to shipping free, set shipping costs to zero */
                       0,
                       /* calculate shippingcosts */
                       IF(
                       /* only calc additional shippingcosts when quantity is 2 or lager */
                       ob.quantity>1,
                       /* first article does not get additional costs so quantity is lowered by 1 */
                       aa.shippingcost+(IFNULL(aa.additionalshippingcosts,0)*(ob.quantity-1)),
                       /* if quantity is 1 only regular shippingcosts are provided */
                       aa.shippingcost
                       )
                  ) AS totalshippingcosts
             FROM s_order_basket ob
                        JOIN s_articles a ON a.id = ob.articleID
                        JOIN s_articles_details d ON d.articleID = a.id AND d.ordernumber = ob.ordernumber
                        JOIN s_articles_attributes aa ON aa.articledetailsID = d.id
                        LEFT JOIN s_articles_supplier_attributes asa ON a.supplierID = asa.supplierID
             -- WHERE aa.attr10 = 2 -- ob.sessionID = @SID
             -- AND aa.attr10 = 2
             ) AS X
         GROUP BY X.supplierID, X.sessionID) as total
    WHERE total.sessionID = b.sessionID
)
