<?php

namespace HrExtendedShippingCosts;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use Exception;
use HrExtendedShippingCosts\Models\ExtendedShippingCosts;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use HrExtendedShippingCosts\Components\HRConst;

class HrExtendedShippingCosts extends Plugin
{
    /**
     * @var ModelManager
     */
    private ModelManager $modelManager;

    /**
     * @var AbstractSchemaManager
     */
    private AbstractSchemaManager $schemaManager;

    /**
     * @var SchemaTool
     */
    private SchemaTool $schemaTool;

    /**
     * @var Connection
     */
    private Connection $db;

    /**
     * @var int
     */
    private int $dispatchID;

    /**
     * @var InstallContext
     */
    private $context;

    /**
     * @param InstallContext $context
     *
     * @throws ToolsException
     * @throws Exception
     */
    public function install(InstallContext $context): void
    {
        $this->context = $context;
        $this->createExtendedShippingCostTables();
        $this->prepareAttributes();
        $this->createShippingCostEntry();
    }

    /**
     * @throws ToolsException
     */
    private function createExtendedShippingCostTables(): void
    {
        $this->setTools();
        $classes = $this->getClasses();

        /** @var  ClassMetadata $class */
        foreach ($classes as $class) {
            if (!$this->schemaManager->tablesExist([$class->getTableName()])) {
                $this->schemaTool->createSchema([$class]);
            } else {
                $this->schemaTool->updateSchema([$class], true);
            }
        }
    }

    /**
     * This method removes the the creates shipping_costs table from the DB
     * Its currently not used since its to dangerous deleting provided data.
     * This method is just implemented for later usage (if necessary)
     */
    private function removeExtendedShippingCostTables()
    {
        $this->schemaTool->dropSchema($this->getClasses());
    }

    /**
     * @throws Exception
     */
    private function prepareAttributes(): void
    {
        //$crud = $this->container->get('shopware_attribute.crud_service');
        if (($crud = $this->container->get('shopware_attribute.crud_service')) !== false) {
            $crud->update(
                HRConst::SHIPPING_COST_ATTR_TABLE,
                HRConst::SHIPPING_COST_ATTR_COLUMN,
                'boolean'
            );
        }
    }

    /**
     * create shipping cost element in shippingcosts only if there is none.
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function createShippingCostEntry(): void
    {
        $this->db = $this->container->get('dbal_connection');
        if (!$this->findActiveExtendedShippingCost()) {
            $this->addShippingCostsElement();
        }
    }

    /**
     * Retrieve the sql statement that has to be insert into the shippingcost calculations
     *
     * @return false|string
     */
    private function getSqlStatement()
    {
        return file_get_contents($this->getPath() . '/Resources/SQL/calculation.sql');
    }

    /**
     * This method is only called if there is no shippingcost.
     * Its identified by a attribute shipping attribute table.
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function addShippingCostsElement(): void
    {
        $this->addMainEntry();
        $this->addAttribute();
        $this->addDetails();
        $this->addCountry();
        $this->addPayments();
    }

    /**
     * This method will add active payments to the shippingcosts entry
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function addPayments(): void
    {
        $paymentIds = $this->getPaymentIds();
        foreach ($paymentIds as $payment) {
            $this->db->insert(
                HRConst::SHIPPING_COST_PAYMENT_TABLE,
                [
                    'dispatchID' => $this->dispatchID,
                    'paymentID'  => $payment['id'],
                ]
            );
        }
    }

    /**
     * adds countries to shippingcosts entry. Currently only german
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function addCountry(): void
    {
        $this->db->insert(
            HRConst::SHIPPING_COST_COUNTRY_TABLE,
            [
                'dispatchID' => $this->dispatchID,
                'countryID'  => 2 //germany
            ]
        );
    }

    /**
     * Set factor to 100% and set price-range from zero to zero what
     * actually means "doesnt matter what price it is
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function addDetails(): void
    {
        $this->db->insert(
            HRConst::SHIPPING_COST_DETAIL_TABLE,
            [
                '`from`'     => 0.000,
                'value'      => 0.000,
                'factor'     => 100.00,
                'dispatchID' => $this->dispatchID,
            ]
        );
    }

    /**
     * This attribute is used to identify the shippingcost entry
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function addAttribute(): void
    {
        $this->db->insert(
            HRConst::SHIPPING_COST_ATTR_TABLE,
            [
                'dispatchID'                       => $this->dispatchID,
                HRConst::SHIPPING_COST_ATTR_COLUMN => 1,
            ]
        );
    }

    /**
     * creates the main entry to shippingcosts elements
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function addMainEntry(): void
    {
        $this->db->insert(
            HRConst::SHIPPING_COST_TABLE,
            [
                'name'                  => HRConst::SHIPPING_COST_NAME,
                'type'                  => 0,
                'description'           => 'Erweiterter Versand mit eigener Berechnung. Versand wird noch Supplier berechnet.',
                'comment'               => '',
                'active'                => 1,
                'position'              => 0,
                'calculation'           => 3, //eigene Berechnung
                'surcharge_calculation' => 0,
                'tax_calculation'       => 0,
                'bind_shippingFree'     => 2,
                'bind_laststock'        => 0,
                'status_link'           => '',
                'calculation_sql'       => $this->getSqlStatement(),
            ]
        );
        $this->dispatchID = $this->db->lastInsertId();
    }

    /**
     * @return mixed
     * @throws \Doctrine\DBAL\Exception
     */
    private function findActiveExtendedShippingCost()
    {
        $queryBuilder = $this->db->createQueryBuilder();

        return $queryBuilder->select("*")->from(HRConst::SHIPPING_COST_ATTR_TABLE)->where(
            HRConst::SHIPPING_COST_ATTR_COLUMN . " = 1"
        )->execute()->fetch();
    }

    /**
     * retrieve all active payment ids
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function getPaymentIds(): array
    {
        $queryBuilder = $this->db->createQueryBuilder();

        return $queryBuilder->select(['id'])->from(HRConst::PAYMENT_TABLE)->where("active = 1")->execute()->fetchAll();
    }

    /**
     * @return array
     */
    private function getClasses(): array
    {
        return [
            $this->modelManager->getClassMetadata(ExtendedShippingCosts::class),
        ];
    }

    /**
     * Set some needed variables when installing the plugin
     */
    private function setTools(): void
    {
        if (!$this->modelManager) {
            $this->modelManager = $this->container->get("models");
        }

        if (!$this->schemaTool) {
            $this->schemaTool = new SchemaTool($this->modelManager);
        }

        if (!$this->schemaManager) {
            $this->schemaManager = $this->modelManager->getConnection()->getSchemaManager();
        }
    }
}
