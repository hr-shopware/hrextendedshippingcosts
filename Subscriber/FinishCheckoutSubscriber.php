<?php

namespace HrExtendedShippingCosts\Subscriber;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Enlight\Event\SubscriberInterface;
use HrExtendedShippingCosts\Components\HRConst;
use HrExtendedShippingCosts\Models\ExtendedShippingCosts;
use Shopware\Bundle\StoreFrontBundle\Struct\Attribute;
use Shopware\Components\Model\ModelManager;

class FinishCheckoutSubscriber implements SubscriberInterface
{
    /**
     * @var ModelManager
     */
    private ModelManager $modelManager;

    /**
     * @var array
     */
    private array $resultCache = [];

    /**
     * OrderBasketSubscriber constructor.
     *
     * @param ModelManager $modelManager
     */
    public function __construct(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_Controllers_Frontend_Checkout::finishAction::after' => 'onFinishCheckout',
        ];
    }

    /**
     * @throws OptimisticLockException
     */
    public function onFinishCheckout(): void
    {
        $this->collectOrderData();

        $this->prepareBeforeStore();

        $this->persistShippingInfos();
    }

    private function collectOrderData(): void
    {
        $orderDetails  = $this->getOrderDetails();
        $sOrderNumber  = $orderDetails['sOrderNumber'];
        $basket        = $orderDetails['sBasket'];
        $basketDetails = $basket['content'];

        foreach ($basketDetails as $articleInfo) {
            $additionalInfo                          = $this->getAdditionalInfos($articleInfo);
            $data['order_number']                    = $sOrderNumber;
            $data['article_id']                      = $articleInfo['articleID'];
            $data['shipping_free']                   = $articleInfo['shippingfree'];
            $data['article_amount']                  = $articleInfo['quantity'];
            $data['order_date']                      = $articleInfo['datum'];
            $data['shipping_costs']                  = $additionalInfo['shipping_costs'];
            $data['shipping_costs_additional']       = $additionalInfo['additional_shipping_costs_per_article'];
            $data['shipping_free_from']              = $additionalInfo['shipping_free_from'];
            $data[HRConst::ADDING_UP_SHIPPING_COSTS] = $additionalInfo[HRConst::ADDING_UP_SHIPPING_COSTS];
            $data['shipping_costs_total']            = $this->calculateTotalShippingCosts($data);
            $data['supplier_number']                 = $additionalInfo['supplier_number'];
            if (!$data['supplier_number']) {
                continue;
            }

            $this->cacheResults($articleInfo, $data);
        }
    }

    /**
     * @param $articleInfo
     * @param $data
     */
    private function cacheResults($articleInfo, $data): void
    {
        $id                                                        = $data['supplier_number'];
        $this->resultCache[$id]['total']                           += $articleInfo['price'] * $articleInfo['quantity'];
        $this->resultCache[$id]['free_from']                       = $data['shipping_free_from'];
        $this->resultCache[$id][HRConst::ADDING_UP_SHIPPING_COSTS] = $data[HRConst::ADDING_UP_SHIPPING_COSTS];
        $this->resultCache[$id]['articles'][]                      = $data;
    }

    /**
     * @return mixed
     */
    private function getOrderDetails()
    {
        $session = Shopware()->Session();

        return $session['sOrderVariables'];
    }

    /**
     * @param array $articleInfo
     *
     * @return array
     */
    private function getAdditionalInfos(array $articleInfo): array
    {
        $additionalDetails     = $articleInfo['additional_details'];
        $addingUpShippingCosts = 0;
        $shippingFreeFrom      = 0;
        /** @var Attribute $attr */
        $attr = $additionalDetails['supplier_attributes']['core'];
        if ($attr) {
            $addingUpShippingCosts = $attr->get(HRConst::ADDING_UP_SHIPPING_COSTS);
            $shippingFreeFrom      = $attr->get(HRConst::SHIPPING_FREE_FROM);
        }

        return [
            'shipping_costs'                        => $additionalDetails[HRConst::SHIPPING_COSTS],
            'shipping_free_from'                    => $shippingFreeFrom,
            'additional_shipping_costs_per_article' => $additionalDetails[HRConst::ADDITIONAL_SHIPPING_COST],
            HRConst::ADDING_UP_SHIPPING_COSTS       => $addingUpShippingCosts,
            'supplier_number'                       => $additionalDetails['supplierID'],
        ];
    }

    /**
     * @param array $data
     *
     * @return float
     */
    private function calculateTotalShippingCosts(array $data): float
    {
        $totalShippingCosts = 0.00;
        if (!$data['shipping_free']) {
            $totalShippingCosts = $data['shipping_costs'];
            if ($data['shipping_costs_additional'] > 0.00 && $data['article_amount'] > 1) {
                for ($i = 1; $i < $data['article_amount']; $i++) {
                    $totalShippingCosts += $data['shipping_costs_additional'];
                }
            }
        }

        return empty($totalShippingCosts)?0.00:$totalShippingCosts;
    }

    private function prepareBeforeStore(): void
    {
        /* @todo hier muss ich die versandkosten prüfen und ggf auf 0 setzen wenn addingupshippingcosts auf 0 ist */

        foreach ($this->resultCache as $supplier => $record) {
            // set shipping to zero if total exceeds free from limit
            if ($record['free_from'] > 0 && $record['total'] > $record['free_from']) {
                foreach ($record['articles'] as $key => $article) {
                    $this->resultCache[$supplier]['articles'][$key]['shipping_costs_total'] = 0.00;
                }
                continue;
            }

            // use highest shipping costs
            if(!$record[HRConst::ADDING_UP_SHIPPING_COSTS]){
                $col = array_column($record['articles'], 'shipping_costs_total');
                $highest = array_keys($col, max($col));

                foreach ($record['articles'] as $key => $article) {
                    if($key !== $highest[0]){
                        $this->resultCache[$supplier]['articles'][$key]['shipping_costs_total'] = 0.00;
                    }
                }
            }
        }
    }

    /**
     * @throws OptimisticLockException|ORMException
     */
    private function persistShippingInfos(): void
    {
        foreach ($this->resultCache as $record) {
            foreach ($record['articles'] as $shippingData) {
                $extendedShippingCosts = new ExtendedShippingCosts();
                if ($extendedShippingCosts->setExtendedShippingCosts($shippingData)) {
                    $this->modelManager->persist($extendedShippingCosts);
                    $this->modelManager->flush($extendedShippingCosts);
                }
            }
        }
    }
}
