<?php

namespace HrExtendedShippingCosts\Models;

use Shopware\Components\Model\ModelEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Table(name="hr_extended_shipping_costs")
 * @ORM\Entity()
 */
class ExtendedShippingCosts extends ModelEntity
{
    /**
     * Primary Key - autoincrement
     *
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $order_number
     *
     * @ORM\Column(name="order_number", type="integer", nullable=false)
     */
    private $order_number;

    /**
     * @var integer $article_id
     *
     * @ORM\Column(name="article_id", type="integer", nullable=false)
     */
    private $article_id;

    /**
     * @var integer $article_amount
     *
     * @ORM\Column(name="article_amount", type="integer", nullable=false)
     */
    private $article_amount;

    /**
     * @var float $shipping_costs
     *
     * @ORM\Column(name="shipping_costs", type="float", nullable=false)
     */
    private $shipping_costs;

    /**
     * @var float $shipping_costs_additional
     *
     * @ORM\Column(name="shipping_costs_additional", type="float", nullable=false)
     */
    private $shipping_costs_additional;

    /**
     * @var float $shipping_costs_total
     *
     * @ORM\Column(name="shipping_costs_total", type="float", nullable=false)
     */
    private $shipping_costs_total;

    /**
     * @var integer $supplier_number
     *
     * @ORM\Column(name="supplier_number", type="integer", nullable=false)
     */
    private $supplier_number;

    /**
     * @var datetime $order_date
     *
     * @ORM\Column(name="order_date", type="datetime", nullable=false)
     */
    private $order_date;

    /**
     * @return DateTime
     */
    public function getOrderDate(): DateTime
    {
        return $this->order_date;
    }

    /**
     * @param DateTime $order_date
     */
    public function setOrderDate($order_date)
    {
        $this->order_date = $order_date;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderNumber()
    {
        return $this->order_number;
    }

    /**
     * @param int $order_number
     */
    public function setOrderNumber($order_number)
    {
        $this->order_number = $order_number;
    }

    /**
     * @return int
     */
    public function getArticleId()
    {
        return $this->article_id;
    }

    /**
     * @param int $article_id
     */
    public function setArticleId($article_id)
    {
        $this->article_id = $article_id;
    }

    /**
     * @return int
     */
    public function getArticleAmount()
    {
        return $this->article_amount;
    }

    /**
     * @param int $article_amount
     */
    public function setArticleAmount($article_amount)
    {
        $this->article_amount = $article_amount;
    }

    /**
     * @return float
     */
    public function getShippingCosts()
    {
        return $this->shipping_costs;
    }

    /**
     * @param float $shipping_costs
     */
    public function setShippingCosts($shipping_costs)
    {
        $this->shipping_costs = $shipping_costs;
    }

    /**
     * @return float
     */
    public function getShippingCostsAdditional()
    {
        return $this->shipping_costs_additional;
    }

    /**
     * @param float $shipping_costs_additional
     */
    public function setShippingCostsAdditional($shipping_costs_additional)
    {
        $this->shipping_costs_additional = $shipping_costs_additional;
    }

    /**
     * @return float
     */
    public function getShippingCostsTotal()
    {
        return $this->shipping_costs_total;
    }

    /**
     * @param float $shipping_costs_total
     */
    public function setShippingCostsTotal($shipping_costs_total)
    {
        $this->shipping_costs_total = $shipping_costs_total;
    }

    /**
     * @return int
     */
    public function getSupplierNumber()
    {
        return $this->supplier_number;
    }

    /**
     * @param int $supplier_number
     */
    public function setSupplierNumber($supplier_number)
    {
        $this->supplier_number = $supplier_number;
    }

    /**
     * @param $tableArray
     *
     * @return bool
     */
    public function setExtendedShippingCosts($tableArray)
    {
        $this->setOrderNumber($tableArray['order_number']);
        $this->setArticleId($tableArray['article_id']);
        $this->setArticleAmount($tableArray['article_amount']);
        $this->setShippingCosts($tableArray['shipping_costs']);
        $this->setShippingCostsAdditional($tableArray['shipping_costs_additional']);
        $this->setShippingCostsTotal($tableArray['shipping_costs_total']);
        $this->setSupplierNumber($tableArray['supplier_number']);
        $this->setOrderDate($tableArray['order_date']);

        return $this->checkConsistency();
    }

    /**
     * @return bool
     */
    private function checkConsistency()
    {
        if (!($this->shipping_costs
            && $this->shipping_costs_additional
            && $this->shipping_costs_total
            && $this->supplier_number)
        ) {
            return false;
        }

        return true;
    }
}

