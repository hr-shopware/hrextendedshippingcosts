<?php

use HrExtendedShippingCosts\Components\Api\Resource\ExtendedShippingCostApi;
use Shopware\Components\Api\Exception\PrivilegeException;
use Shopware\Components\Api\Manager;

class Shopware_Controllers_Api_ShippingCost extends Shopware_Controllers_Api_Rest
{
    /**
     * @var ExtendedShippingCostApi
     */
    protected $resource = null;

    /**
     *
     */
    public function init()
    {
        $this->resource = Manager::getResource('ExtendedShippingCostApi');
    }

    /**
     * Get list of 1000 ShippingCost
     *
     * GET /api/shippingcost/
     *
     * @throws PrivilegeException
     */
    public function indexAction()
    {
        $request = $this->Request();
        $month   = $request->getParam('month');
        $year    = $request->getParam('year', date('Y'));

        if ($month) {
            $shippingCosts = $this->resource->getList($month, $year);
            $this->View()->assign('data', $shippingCosts);
            $this->View()->assign('success', true);
        } else {
            $this->View()->assign('success', false);
        }
    }
}
